# Drupal views file descriptions



## Goal

A view in Drupal 7 that displays file details including file descriptions for a given content type that has a file field.


## Solution overview

Create a view that **Show**s *Files*. Specify the type of files if desired.

- [ ] Add a **Relationship** for **File:Content using File** (there may be several, choose the one that makes sense for your file field)
- [ ] Add field for **File: File ID (File ID)** to the view so you have access to the *fid*. If you don't need to display it, check **Exclude from display**.
- [ ] Add other file fields as required, e.g. **File: Name**, **File: Mimetype**, **File: Type**, **File: Upload date**, **File: Path**, **File: UUID** (**Universally Unique ID** module). Note: If you are using **File: Upload date** for a migration a good date format is *Y-m-d G:i:s*.
- [ ] Use the (field_file) relationship to display parent fields like **Content: Author uid**, **Content: Title (Parent title)**, **Content: Type (Parent content type)**, **Content: Nid (Parent nid)**
- [ ] Create another view named *file_description* with a display named *page* to show the content type's file field (show Content of type Project, for example), add the **Content: File** to the **Fields**; set **Rewrite results** to *[field_file-fid] : [field_file-description]*; add **Contextual filters**: **Content: File:fid** with **Type** *Fixed value* and **Fixed value** of *%1*, **Global: Null** filter with **Administrative title** of *Nid*, another **Global: Null** filter with **Admin title** of *Delta*.
- [ ] In the first view, add a **Global: PHP field** (**Views PHP** module)
- [ ] Set **Value code** like: `return views_embed_view('file_description', 'page', $row->fid);`
- [ ] Set **Output code** to: `<?php echo $value; ?>`

Test, and once working, you can remove *"[field_file-fid] : "* from the **Rewrite results** of view *file_description*. That's just there to verify it's wired as expected.

If you could add a view field formatter (**Views field formatter** module) to the **File: File ID** field, then you could use the *file_description* view as the **File ID** formatter and skip the PHP field, but the **File ID** field does not appear to support a **Formatter**.

A view that shows file details is useful when performing site migrations. Having access to PHP in views is essential when the correct solution, e.g. **Views field formatter** will not work.

## Extra

If you get this working, for extra credit duplicate the file_description page as a page and name the first page_desc and the second page_display and update the File field to show field_file-display.

## Followup

Using a page display producees output like

```<File-display><div class="view view-file-description view-id-file_description view-display-id-page_display view-dom-id-76f52f4e14259d456bddcc6a615eeb46"> <div class="view-content"> <div> 1 </div> </div> </div></File-display>```

where it is wrapped with the views tags and classes. To remove these in the *file_description* view, use a display of *data export* and set **Format** to *CSV File* (**Views data export** module). Update the CSV settings until you get clean text. Note that there will be a trailing newline (\n) in the CSV output. This must be trimmed by the receiver.

For instance, in a migration yml, trim the description like so:

```yml
  field_media_file/description:
    - plugin: skip_on_empty
      method: process
      source: file_description
    - plugin: callback
      callable: trim
```

## Followup II

The file description is not shown if the file display is not 1 (checked). Use a db query instead.

Ex.

SELECT field_file_description FROM `field_data_field_file` where field_file_fid = 3021;

Views PHP field **Value code**:

```php
$desc = db_query('select field_file_description from `field_data_field_file` where field_file_fid = ' . $row->fid)->fetchColumn();
return $desc;
```

For file display use

```php
$display = db_query('select field_file_display from `field_data_field_file` where field_file_fid = ' . $row->fid)->fetchColumn();
return $display;
```

## Reference

```php 
/**
 * Embed a view using a PHP snippet.
 *
 * This function is meant to be called from PHP snippets, should one wish to
 * embed a view in a node or something. It's meant to provide the simplest
 * solution and doesn't really offer a lot of options, but breaking the function
 * apart is pretty easy, and this provides a worthwhile guide to doing so.
 *
 * Note that this function does NOT display the title of the view. If you want
 * to do that, you will need to do what this function does manually, by
 * loading the view, getting the preview and then getting $view->get_title().
 *
 * @param string $name
 *   The name of the view to embed.
 * @param string $display_id
 *   The display id to embed. If unsure, use 'default', as it will always be
 *   valid. But things like 'page' or 'block' should work here.
 * @param ...
 *   Any additional parameters will be passed as arguments.
 */
function views_embed_view($name, $display_id = 'default') {
  $args = func_get_args();
  // Remove $name.
  array_shift($args);
  if (count($args)) {
    // Remove $display_id.
    array_shift($args);
  }

  $view = views_get_view($name);
  if (!$view || !$view->access($display_id)) {
    return;
  }

  return $view->preview($display_id, $args);
}


/**
 * This is how Views field formatter renders a view.
 * 
 * Implements hook_field_formatter_view().
 */
function views_field_formatter_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  if (!empty($items)) {
    $settings = $display['settings']['views_field_formatter'];
    list($view, $view_display) = explode('::', $settings['view']);

    $field_columns = array_keys($field['columns']);
    $index = array_shift($field_columns);

    $entity_info = entity_get_info($entity_type);
    $key = $entity_info['entity keys']['id'];

    if (isset($field['cardinality']) && ($field['cardinality'] != 1) && (boolval($settings['multiple']) == TRUE)) {
      if (!empty($settings['implode_character'])) {
        $values = array();
        foreach ($items as $item) {
          $values[] = isset($item[$index]) ? $item[$index] : NULL;
        }
        $value = implode($settings['implode_character'], array_filter($values));
        $element[0] = array(
          '#markup' => views_embed_view($view, $view_display, $value, $entity->$key, 0),
        );
      }
      else {
        foreach ($items as $delta => $item) {
          $value = isset($item[$index]) ? $item[$index] : NULL;
          $element[$delta] = array(
            '#markup' => views_embed_view($view, $view_display, $value, $entity->$key, $delta),
          );
        }
      }
    }
    else {
      $value = isset($items[0][$index]) ? $items[0][$index] : NULL;
      $element[0] = array(
        '#markup' => views_embed_view($view, $view_display, $value, $entity->$key, 0),
      );
    }
  }

  return $element;
}

```